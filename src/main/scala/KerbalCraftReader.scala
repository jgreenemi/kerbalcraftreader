import java.io.File

import scala.io.Source

/**
  * A simple utility to enumerate the metadata regarding your Kerbal Space Program builds.
  */

object KerbalCraftReader {
  val saves_dir = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Kerbal Space Program\\saves"
  val ships_dir = "Ships\\VAB"

  def get_saves(): List[String] = {
    val dir = new File(saves_dir)
    if (dir.exists && dir.isDirectory) {
      dir.listFiles.filter(_.isDirectory).toList.map(_.toString)
    } else {
      List[String]()
    }
  }

  def get_craft_files(save_dir: String): List[File] = {
    val dir = new File(save_dir)
    val exclusions: List[String] = List(
      "scenarios",
      "training"
    )
    if (dir.exists && dir.isDirectory) {
      dir.listFiles
        .filter(_.isFile)
        .toList
        .filterNot {
          file =>
            exclusions.exists(file.getName.endsWith(_))
        }
    } else {
      List[File]()
    }
  }

  def craft_file_reader(file: File): Unit = {
    for (line <- Source.fromFile(file).getLines) {

      if (line contains "ship = ") println(s"Ship: ${line.substring(7)}")
      else if (line contains "description = ") {
        // KSP uses a certain character to denote newlines. We'll interpret that here.
        println(s"Description:\n${line.replace("¨", "\n").substring(14)}")
      }
    }
  }

  def main(args: Array[String]): Unit = {
    for (save_file_name <- get_saves()) {
      val craft_file_dir = s"$save_file_name\\$ships_dir"
      val craft_files = get_craft_files(craft_file_dir)
      if (craft_files.nonEmpty) println(s"Found ${craft_files.length} rockets for save ${save_file_name.substring(saves_dir.length)}.")
      for (file <- craft_files) {
        craft_file_reader(file)
      }
    }
  }
}
